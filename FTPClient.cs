﻿﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;

namespace FTPClient
{
    class client
    {
        static void Main(string[] args)
        {
        	//connecting to server
            Console.Write(" Enter server Ip Address : ");
            string ipAddr = Console.ReadLine();
            TcpClient tcpCli = new TcpClient(ipAddr, 6000);
            StreamWriter streamwriter = new StreamWriter(tcpCli.GetStream());

            //if its connected
            try
            {
            	//input file directory
            	Console.Write(" Enter file directory to send : ");
            	string directory = Console.ReadLine();

                Console.WriteLine(" Sending files...");
                byte[] bytes = File.ReadAllBytes(directory);

                tcpCli.Client.SendFile(directory);
                Console.WriteLine(" File sent");
            }
            //if its not connected
            catch (Exception e)
            {
            	//show errors
                Console.Write(e.Message);
            }

            Console.Read();
        }
    }
}
